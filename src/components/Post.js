import React, {Component} from 'react';
import User from './User'

export default class Post extends Component {
    render() {
        return (
            <div className="post">
                <User 
                    src="https://avatars.mds.yandex.net/get-pdb/2301590/5b3f1573-deaf-497b-87f5-dbae23ae0ca6/s1200" 
                    alt="man" 
                    name="DDD"
                    min/>
                <img src={this.props.src} alt={this.props.alt}></img>
                <div className="post__name">
                    some account
                </div>
                <div className="post__descr">
                    some description
                </div>
            </div>
        )
    }
}