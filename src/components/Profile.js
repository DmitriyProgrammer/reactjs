import React from 'react';
import User from './User'
import Palette from './Palette';

const Profile = () => {
    return (
        <div className="container profile">
            <User 
                src="https://avatars.mds.yandex.net/get-pdb/2301590/5b3f1573-deaf-497b-87f5-dbae23ae0ca6/s1200" 
                alt="man" 
                name="DDD"/>
            <Palette/>
        </div>
    )
}
export default Profile;